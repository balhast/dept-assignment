import type { NextPage } from "next";
import { HomeTemplate } from "../components/templates";

const Home: NextPage = () => {
  return <HomeTemplate />;
};

export default Home;
