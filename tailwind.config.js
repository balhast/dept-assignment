module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'homepage': "url('/images/florensis.png')",
        'bolcom': "url('/images/bolcom')",
        'kempen': "url('/images/kempen.png')",
        'philips': "url('/images/philips.png')",
        'gemeentemuseum': "url('/images/gemeentemuseum.png')",
        'lightning': "url('/images/lightning.png')",
        'tui': "url('/images/tui.png')",
        'chocomel': "url('/images/chocomel.png')",
        'jbl': "url('/images/jbl.png')",
        'zalando': "url('/images/zalando.png')",
        'koninklijke-bibliotheek': "url('/images/koninklijke-bibliotheek.png')"
      }
    },
    minWidth: {
      '1/2': '50%',
      '2/5': '40%'
    },
  },
  plugins: [],
}
