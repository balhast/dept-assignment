import * as React from "react";
import { ClientsText } from "../../atoms";
import { Cases, Footer } from "../../molecules";
import { MainHome } from "../../organisms";

const HomeTemplate: React.FC = () => {
  return (
    <div className="">
      <div className="h-screen bg-white p-5">
        <MainHome />
      </div>
      <div className="pb-5">
        <Cases />
      </div>
      <div className="flex justify-center pb-14 pt-10 bg-gray-100">
        <ClientsText />
      </div>
      <Footer />
    </div>
  );
};

export default HomeTemplate;
