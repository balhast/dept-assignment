import * as React from "react";
import { Logo } from "../../../atoms";

type Props = React.HTMLProps<HTMLDivElement>;

const FooterNav: React.FC<Props> = (props) => {
  return (
    <div
      {...props}
      className="flex grid grid-cols-2 border-b border-gray-600 pt-12 pb-6"
    >
      <div>
        <Logo />
        <div className="text-sm">
          <ul>
            <li>WORK</li>
            <li>SERVICES</li>
            <li>STORIES</li>
            <li>ABOUT</li>
            <li>CAREERS</li>
            <li>CONTACT</li>
          </ul>
        </div>
      </div>
      <div className="flex justify-end items-center">
        <p>iconsssss</p>
      </div>
    </div>
  );
};

export default FooterNav;
