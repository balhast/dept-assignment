import classNames from "classnames";
import * as React from "react";
import { FooterNav } from "..";
import { FooterText } from "../../atoms";

type Props = React.HTMLProps<HTMLElement>;

const Footer: React.FC<Props> = ({ className, ...rest }) => {
  return (
    <footer {...rest} className={classNames("bg-black text-white", className)}>
      <div className="mx-32 ">
        <FooterNav />
        <FooterText />
      </div>
    </footer>
  );
};

export default Footer;
