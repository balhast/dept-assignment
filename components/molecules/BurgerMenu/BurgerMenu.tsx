import * as React from "react";

import { Burger, Menu } from "../../atoms";

type Props = React.HTMLProps<HTMLDivElement> & {
  showMenu: boolean;
  toggleMenu: () => void;
};

const BurgerMenu: React.FC<Props> = ({ showMenu, toggleMenu, ...rest }) => (
  <div {...rest}>
    <Burger onClick={toggleMenu} />
    {showMenu && <Menu toggleMenu={toggleMenu} />}
  </div>
);

export default BurgerMenu;
