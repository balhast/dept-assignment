import * as React from "react";
import BurgerMenu from "./BurgerMenu";

type Props = React.HTMLProps<HTMLDivElement>;

const BurgerMenuContainer: React.FC<Props> = (props) => {
  const [showMenu, setShowMenu] = React.useState<boolean>(false);

  const toggleMenu = React.useCallback(() => {
    setShowMenu(!showMenu);
  }, [showMenu]);

  return <BurgerMenu {...props} showMenu={showMenu} toggleMenu={toggleMenu} />;
};

export default BurgerMenuContainer;
