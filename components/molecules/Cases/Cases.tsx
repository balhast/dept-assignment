import Image from "next/image";
import React from "react";
import { Button, Name } from "../../atoms";
import items from "./cases-items";

type Props = React.HTMLProps<HTMLDivElement>;

const Cases: React.FC<Props> = ({ className, ...rest }) => {
  return (
    <div {...rest} className="flex justify-center mt-20">
      <div className="grid grid-cols-2 gap-6 max-w-8xl">
        {items.map(({ name, classPostfix, label }) => (
          <div className="flex-1 basis-full" key={name}>
            <div className="h-96 object-fit relative h-[429px] w-[502.5px]">
              <Image
                src={`/images/${classPostfix}.png`}
                width={335}
                height={286}
                layout="fill"
              />
            </div>
            <Name caption={`${name}`} />
            <label className="text-xl">{label}</label>
            <Button
              className="flex px-0 bg-white text-blue-700 text-xs font-bold"
              caption="VIEW CASE"
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Cases;
