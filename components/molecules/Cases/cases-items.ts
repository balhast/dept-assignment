const items = [
  {
    name: "bolcom",
    classPostfix: "bolcom",
    label: "A summer island in the Netherlands",
  },
  {
    name: "kempen",
    classPostfix: "kempen",
    label: "Not some average banking website",
  },
  {
    name: "philips",
    classPostfix: "philips",
    label: "Beautiful design meets innovative technology",
  },
  {
    name: "gemeentemuseum",
    classPostfix: "gemeentemuseum",
    label: "A 100 years of Mondriaan & De Stijl",
  },
  {
    name: "lightning",
    classPostfix: "lightning",
    label: "Delivering clarity on a global scale",
  },
  {
    name: "tui",
    classPostfix: "tui",
    label: "Swipe to find your next holiday destination",
  },
  {
    name: "chocomel",
    classPostfix: "chocomel",
    label: "A campaign for the limited edition letter packs",
  },
  {
    name: "jbl",
    classPostfix: "jbl",
    label: "Live like a champion with Jerome Booteng",
  },
  {
    name: "zalando",
    classPostfix: "zalando",
    label: "Innovative SEO and content strategy for Zalando",
  },
  {
    name: "koninklijke-bibliotheek",
    classPostfix: "koninklijke-bibliotheek",
    label: "The search for the most influential book ever",
  },
]

export default items
