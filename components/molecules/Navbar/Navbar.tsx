import * as React from "react";

import { Logo } from "../../atoms";
import { BurgerMenu } from "../../molecules";

const Navbar: React.FC = () => {
  return (
    <div className="flex grid grid-cols-2 border-b border-black">
      <Logo className="pb-8" />
      <div className="flex justify-end pb-2">
        <BurgerMenu />
      </div>
    </div>
  );
};

export default Navbar;
