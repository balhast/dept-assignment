export type MenuItem = {
  name: string
  href: string
}

const menuItems: MenuItem[] = [{
  name: 'Home',
  href: '/'
}, {
  name: 'Werk',
  href: '/werk'
}, {
  name: 'Over',
  href: '/over'
}, {
  name: 'Diensten',
  href: '/diensten'
}, {
  name: 'Partners',
  href: '/partners'
}, {
  name: 'Stories',
  href: '/stories'
}, {
  name: 'Vacatures',
  href: '/vacatures'
}, {
  name: 'Events',
  href: '/events'
}]

export default menuItems