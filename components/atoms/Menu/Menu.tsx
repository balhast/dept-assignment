import * as React from "react";
import classNames from "classnames";

import MenuItem from "./MenuItem/MenuItem";
import menuItems from "./menu-items";

type Props = React.HTMLAttributes<HTMLElement> & {
  toggleMenu: () => void;
};

const Menu: React.FC<Props> = ({ className, toggleMenu, ...rest }) => {
  return (
    <nav
      {...rest}
      className={classNames(
        "fixed top-0 left-0 w-full h-full bg-white p-5 z-50",
        className
      )}
    >
      <div className="bg-black w-full h-full px-[128px] flex flex-col items-end">
        <button className="text-white pt-12" onClick={toggleMenu}>
          close
        </button>
        <div className="pt-[100px]">
          <div className="flex flex-col items-end">
            {menuItems.map((menuItem) => (
              <MenuItem key={menuItem.name} {...menuItem} />
            ))}
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Menu;
