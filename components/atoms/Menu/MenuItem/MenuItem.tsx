import * as React from "react";
import classNames from "classnames";

type Props = React.HTMLProps<HTMLAnchorElement> & {
  name: string;
};

const MenuItem: React.FC<Props> = ({ className, name, ...rest }) => (
  <a
    {...rest}
    className={classNames(
      className,
      "border-b border-gray-500 max-w-[560px] w-full text-right text-7xl text-white uppercase"
    )}
  >
    {name}
  </a>
);

export default MenuItem;
