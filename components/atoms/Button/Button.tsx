import * as React from "react";
import classNames from "classnames";

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  caption: string;
};

const Button: React.FC<Props> = ({ caption, className, ...rest }) => {
  return (
    <button
      {...rest}
      className={classNames("px-10 py-3 bg-black text-white", className)}
    >
      {caption}
    </button>
  );
};

export default Button;
