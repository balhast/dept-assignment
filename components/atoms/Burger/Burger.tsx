import * as React from "react";
import classNames from "classnames";

type Props = React.HTMLProps<HTMLButtonElement> & {
  type?: "button" | "submit" | "reset";
};

const Burger: React.FC<Props> = ({ className, ...rest }) => (
  <button {...rest} className={classNames("cursor-pointer mt-3", className)}>
    <div className="w-6 h-0.5 mb-2 bg-black" />
    <div className="w-6 h-0.5 bg-black" />
  </button>
);

export default Burger;
