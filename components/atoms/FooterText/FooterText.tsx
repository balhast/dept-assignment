import * as React from "react";

const FooterText: React.FC = () => {
  return (
    <div className="grid grid-cols-2">
      <div className="grid grid-cols-3">
        <p className="text-xs pt-16 pb-20 text-gray-500">
          Chamber of Commerce: 63464101
        </p>
        <p className="text-xs pt-16 pb-20 text-gray-500">
          VAT: NL 8552.47.502.B01
        </p>
        <p className="text-xs pt-16 pb-20 text-gray-500">
          Terms and conditions
        </p>
      </div>
      <div>
        <p className="text-xs pt-16 pb-20 text-gray-500 flex justify-end">
          © 2018 Dept Agency
        </p>
      </div>
    </div>
  );
};

export default FooterText;
