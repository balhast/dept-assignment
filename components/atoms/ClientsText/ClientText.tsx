import classNames from "classnames";
import * as React from "react";

type Props = React.HTMLProps<HTMLDivElement>;

const ClientsText: React.FC<Props> = ({ className, ...rest }) => {
  return (
    <div {...rest} className={classNames("flex flex-col w-[560px]", className)}>
      <h1 className="uppercase font-medium text-6xl text-center pb-6">
        Clients
      </h1>
      <p className="text font-light text-center">
        We value a great working relationship with our clients above all else.
        It’s why they often come to our parties. It’s also why we’re able to
        challenge and inspire them to reach for the stars.
      </p>
    </div>
  );
};

export default ClientsText;
