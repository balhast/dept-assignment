export { default as Button } from "./Button/Button";
export { default as Burger } from "./Burger/Burger";
export { default as Menu } from "./Menu/Menu";
export { default as Logo } from "./Logo/Logo";
export { default as Name } from "./Name/Name";
export { default as ClientsText } from "./ClientsText/ClientText";
export { default as FooterText } from "./FooterText/FooterText";
