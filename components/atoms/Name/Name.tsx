import classNames from "classnames";
import * as React from "react";

type Props = React.HTMLAttributes<HTMLHeadingElement> & {
  caption: string;
};

const Name: React.FC<Props> = ({ className, caption, ...rest }) => {
  return (
    <h5
      {...rest}
      className={classNames(
        "pt-4 pb-1 text-xs text-gray-500 uppercase",
        className
      )}
    >
      {caption}
    </h5>
  );
};

export default Name;
