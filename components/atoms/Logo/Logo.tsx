import classNames from "classnames";
import React from "react";

type Props = React.HTMLProps<HTMLElement>;

const Logo: React.FC<Props> = (className, ...rest) => {
  return (
    <h1 {...rest} className={classNames(className, "text-2xl font-bold mb-3")}>
      DEPT
    </h1>
  );
};

export default Logo;
