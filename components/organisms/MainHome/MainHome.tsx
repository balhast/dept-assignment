import * as React from "react";

import { Button } from "../../atoms";
import { Navbar } from "../../molecules";

const MainHome: React.FC = () => {
  return (
    <div className="bg-homepage bg-cover h-full">
      <div className="h-full mx-32 py-10 flex flex-col justify-between">
        <Navbar />
        <Button className="w-fit self-end" caption="VIEW CASE" />
      </div>
    </div>
  );
};

export default MainHome;
